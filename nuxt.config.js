require('dotenv').config()

export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    titleTemplate: '%s - ' + 'tiendatleo nuxt starter project',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    htmlAttrs: {
      // lang: 'vi',
    },
  },
  /**
   * env
   */
  env: {
    BASE_URL: process.env.BASE_URL,
    PORT: process.env.PORT,
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /**
   * @nuxtjs/style-resources sources
   */
  styleResources: {
    scss: [
      "~assets/scss/abstracts/_variables.scss",
      "~assets/scss/abstracts/_functions.scss",
      "~assets/scss/abstracts/_mixins.scss",
      "~assets/scss/abstracts/*.scss",
      "~assets/scss/vendors/*.scss"
    ]
  },
  /*
  ** Global CSS
  */
  css: [
    '~assets/scss/styles.scss'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/axios',
    '~/plugins/repositories',
    '~/plugins/global',
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    // Doc: https://github.com/nuxt-community/style-resources-module
    '@nuxtjs/style-resources',
    // Doc: https://github.com/nuxt-community/svg-module
    "@nuxtjs/svg"
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    baseURL: process.env.BASE_URL, // Default: http://[HOST]:[PORT][PREFIX]
    redirectError: {
      404: '/404'
    },
    retry: false, // interceptor retry time request
    debug: false
  },
  /**
   ** Server configuration 
   */
  server: {
    port: process.env.PORT, // default: 3000
    host: '0.0.0.0' // default: localhost
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    },

    /**
     ** Config or css prefix version
     */
    postcss: {
      plugins: {
        'autoprefixer': { overrideBrowserslist: 'last 2 versions' }
      }
    },
  }
}
