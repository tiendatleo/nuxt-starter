export default function ({ store, $axios, redirect }) {
  $axios.onRequest(config => {
    console.log('Making request to ', config.url)
  })

  $axios.onError(error => {
    const code = parseInt(error.response && error.response.status)
    if (code === 400) {
      // redirect('/400')
    }
  })

  $axios.onRequestError(error => {
    // console.log('onRequestError', error)
  })

  $axios.onResponseError(error => {
    // const code = parseInt(error.response && error.response.status)
    // if (code === 401) {
    //   removeToken();
    //   redirect('/login');
    // }
  })

  $axios.onError(error => {
    console.log('onError', error)
  })

  $axios.onResponse(response => {
    // console.log('onResponse', response)
  })
}