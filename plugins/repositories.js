import createRepository from '~/apis/repository.js'

export default (ctx, inject) => {
  const repositoryWithAxios = createRepository(ctx.$axios)

  const repositories = {
    // items: repositoryWithAxios('/web/items'),
  }

  inject('repositories', repositories)
}
